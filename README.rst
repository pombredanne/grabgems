Goal
====

gems downloader for static analysis and other purposes

Usage
=====

*  Run fetch_index.sh

   ::

     $ ./fetch_index.sh

*  Run grabgems.py

   ::

     $ python grabgems.py

Downloaded data is in rubygems/ folder
