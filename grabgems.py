#!/usr/bin/env python

from __future__ import print_function
import os
import sys
import json
from colorama import init
init()
from colorama import Fore, Back, Style
import requests
import os
import multiprocessing
import threading
import sys


blurb = """
$ pip install requests
"""

try:
    import requests
except ImportError as exc:
    print(exc)
    print("Dependencies not met, use following steps!")
    print(blurb)
    sys.exit(-1)

BASE_URL = "https://rubygems.org/api/v1/gems/%s.json"

# global stuff
lock = threading.Lock()
total = None
i = 0.0

def fetch(url, destination):
    r = requests.get(url)
    with open(destination, "wb") as f:
        f.write(r.content)

def analyze(pkg):
    url = BASE_URL % pkg
    md = requests.get(BASE_URL % pkg)
    print(Fore.GREEN + "[*] Processing", url)
    try:
        answer = md.json()
    except:
        print(Fore.MAGENTA + "Failed md fetch for", url)
        return

    try:
        url = answer["gem_uri"]
    except:
        print(Fore.MAGENTA + "Failed URI lookup for", url)
        return

    filename = os.path.join("rubygems", os.path.basename(url))
    if os.path.exists(filename):
        print(Fore.YELLOW + "[+] We already have", filename)
    print(Fore.YELLOW + "[*] Saving to", filename)
    fetch(url, filename)

def output_callback(data):
    global i
    with lock:
        i = i + 1
        print(Fore.YELLOW + "[+] %f done ..." % ((i /  total) * 100))
        sys.stdout.flush()

def load():
    global pkgs
    global total
    with open("latest_specs.json") as f:
        data = json.loads(f.read())

    total = len(data)
    pkgs = []
    for item in data:
        gem, _, _ = item
        pkgs.append(gem)

if __name__ == "__main__":
    print("Loading database ...")
    load()

    p = multiprocessing.Pool(16)
    for pkg in pkgs:
        p.apply_async(analyze, (pkg,), callback = output_callback)
        # analyze(pkg)

    p.close()
    p.join()


